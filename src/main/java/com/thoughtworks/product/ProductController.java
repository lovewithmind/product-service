package com.thoughtworks.product;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    private final RabbitTemplate rabbitTemplate;
    private final ProductRepository productRepository;

    public ProductController(RabbitTemplate rabbitTemplate, ProductRepository productRepository) {
        this.rabbitTemplate = rabbitTemplate;
        this.productRepository = productRepository;
    }

    @PostMapping("/products")
    public ResponseEntity<Product> onProductCreate(@RequestBody Product product) {
        productRepository.save(product);
        rabbitTemplate.convertAndSend("test-exchange", "person.created.event", product);
        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }
}
